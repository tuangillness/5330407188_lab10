/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javassist.convert.Transformer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 *
 * @author Nopsompong
 */
public class checkTINPINService {

    public static void main(String[] args) throws Exception {
        String pin = "";

        checkTINPINService PinService = new checkTINPINService();
        PinService.request(pin);
    }

    public void request(String pin) throws Exception {
        SOAPConnectionFactory connectSoap =  SOAPConnectionFactory.newInstance();
        SOAPConnection connect = connectSoap.createConnection();
        
        
        MessageFactory mFac = MessageFactory.newInstance();
        SOAPMessage message = mFac.createMessage();

        SOAPHeader header = message.getSOAPHeader();
        SOAPBody body = message.getSOAPBody();
        header.detachNode();

        SOAPFactory factory = SOAPFactory.newInstance();
        SOAPBodyElement PinService = body.addBodyElement(factory.createName("ServicePIN",
                "ns", "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));

//create Username
        SOAPElement username = PinService.addChildElement(factory.createName("username", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        username.addTextNode("anonymous");

//create Password
        SOAPElement password = PinService.addChildElement(factory.createName("password", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        password.addTextNode("anonymous");

//create PIN and sent to pin on  checkTINPIN
        SOAPElement pinElement = PinService.addChildElement(factory.createName("PIN", "ns",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService"));
        pinElement.addTextNode(pin);
        
        MimeHeaders hd = message.getMimeHeaders();
        hd.addHeader("SOAPAction",
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService/ServicePIN");
        
        message.saveChanges();
        System.out.println("REQUEST:");
        displayMessage(message);

        
        System.out.println("\n");
        
        XTrustProvider.install();
        SOAPConnection conn =
                SOAPConnectionFactory.newInstance().createConnection();
        SOAPMessage response = conn.call(message,
                "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");

        System.out.println("RESPONSE:");

        displayMessage(response);
    }
    private void displayMessage(SOAPMessage message) {
        try {
            TransformerFactory tFac = TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = tFac.newTransformer();
            Source src = message.getSOAPPart().getContent();
            StreamResult result = new StreamResult(System.out);
            transformer.transform(src, result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
